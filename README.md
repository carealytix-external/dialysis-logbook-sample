# Coding assignment for a simplified dialysis logbook

Hello there!

For this assignment, you are supposed to create a simple dialysis logbook with which users can keep track of their weight, wellbeing, and blood values.
In this repository, you'll find a template project where we already prepared a setup with the required dependencies.
It's a React Native project using expo, typescript, native-base, and react-redux. To get started, all you have to do is create a fork of the project and start hacking!  
The following sections provides an overview of the functionalites and the requirements for the app that you should implement.

## App description
### Data
With this app, users should be able to track their potassium values, phosphate values, weight, wellbeing, and itch intensity.  
You can find an overview of the configuration in [this sample API](https://6374c09048dfab73a4e7b7c4.mockapi.io/logbook-configuration/1). In the app, you should fetch the configuration from this sample API. Possible types for entry values are `NUMERIC` and `INTEGER`, and they should support a `minValue` and `maxValue`. `MetadataFields` can either be `NOTES` (Text field/string), or `BEFORE_OR_AFTER_DIALYSIS` (explicit type: selector allowing you to choose between "Before Dialysis", "After Dialysis", and "Arbitrary").

### App design and flow
The start screen of the app should contain an overview of the 5 possible tracking types mentioned above (potassium, phosphate, ...). 
For each type, also display the most recent entry that the user added.  
Additionally, the users should be able to add new entries for each of the 5 mentioned types, respectively. Implement a flow that makes it clear to the user how and for which type they can add a new entry from the start screen:  
For that, create another screen displaying all relevant data for a specific entry type with which the user can create a new entry by speficying the data for the respective type.  
Make sure that the new entry is saved using the `redux-store`, you'll find a pre-configured one in the template. 

Lastly, the users should be able to see a list of all created entries for each type when clicking on it. Create a third screen that loads and lists the existing entries stored in the redux state. Additionally, users should also be able to add a new entry from this screen, with the constraint that it can only be added for the selected type (e.g. if viewing the potassium entries, users should only be able to add a new potassium entry from this screen)

## Technical requirements
You only have to consider developing for `mobile devices` for this sample project. We are using `expo`, so you can easily run it for the simulator if you have XCode installed, or just download Expo Go on your mobile device and run it there by scanning the QR code in the console after starting the project.  
As the template already suggests, the app should be written in `TypeScript`. Furthermore, we want you to use `native-base` components and theming for the frontend. And lastly, data management should be done using `react-redux` - which is already pre-configured in the template.

## Project init
- Create a fork of this repository and clone it
- You can start the app with `yarn start`
- You can either use the iOS Simulator to preview your app or download the Expo Go app for your smartphone and scan the QR-code displayed in your terminal after starting the app
- You are ready to go! Changes are directly visible in Expo Go as fast reload is enabled
