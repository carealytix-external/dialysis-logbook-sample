import { configureStore } from "@reduxjs/toolkit";
import { logbookReducer } from "./logbook-state";

export default configureStore({
  reducer: {
    logbook: logbookReducer,
  },
});
